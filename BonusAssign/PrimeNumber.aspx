﻿<%@ Page Title="Prime Number" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PalindromeString.aspx.cs" Inherits="BonusAssign.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
      <div class="jumbotron">
        <div>
        <asp:Label runat="server">Prime Number Input</asp:Label>
        <asp:TextBox runat="server" ID="inputPrimeNumber"></asp:TextBox>
        <asp:RequiredFieldValidator ID="inputPrimeNumberRequired" runat="server" ControlToValidate="inputPrimeNumber" ErrorMessage="You must enter a value." ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="inputPrimeNumberRegularExpression" runat="server" ControlToValidate="inputPrimeNumber" ErrorMessage="Entered value must be a Number" ValidationExpression="[0-9]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="submitPrimeNumber" Text="Submit Prime Number" runat="server" OnClick="submitPrimeNumber_Click"/>
        </div>
          <br />
          <div id="primeNumberResult" runat="server">
        
          </div>

        </div>
</asp:Content>
