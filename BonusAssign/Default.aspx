﻿<%@ Page Title="Palindrome" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BonusAssign._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <h2><%: Title %></h2>
    <div class="jumbotron">
       
     <div>
        <asp:Label runat="server">String Input</asp:Label>
        <asp:TextBox runat="server" ID="inputString"  placeholder="Enter a only a string "></asp:TextBox>
        <asp:RequiredFieldValidator ID="inputStringRequired" runat="server" ControlToValidate="inputString" ErrorMessage="You must enter a value" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="inputStringRegularExpression" runat="server" ControlToValidate="inputString" ErrorMessage="Entered value should be String only" ValidationExpression="[a-zA-Z ]+$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Button ID="submitString" Text="Submit String" runat="server" OnClick="submitinputString_Click" />
        
    </div>
        <br />
         <div id="stringResult" runat="server"></div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Palindrome</h2>
        
        </div>
        <div class="col-md-4">
            <h2>Prime Number</h2>
            
        </div>
        <div class="col-md-4">
            <h2>Cartesian</h2>
   
        </div>
    </div>

</asp:Content>
