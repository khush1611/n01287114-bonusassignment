﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssign
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
//******************************** Palindrom******************************//
        protected void submitinputString_Click(object sender, EventArgs e)
        {
           
            if (!Page.IsValid)
            {
                return;
            }

            string input = inputString.Text;
            string inputLowerCase = input.ToLower();
            string inputWithNoSpaces = inputLowerCase.Replace(" ", string.Empty);
            string inputReverse = string.Empty;

            int length = inputWithNoSpaces.Length;
            bool flag = false;
            for (int i = 0; i < length / 2; i++)
            {
                if (inputWithNoSpaces[i] != inputWithNoSpaces[length - i - 1])
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                stringResult.InnerHtml = "<b><em>Palindrome Outcome:</em></b>  <strong>" + input + "</strong> yes,Palindrome string";
            }
            else
            {
                stringResult.InnerHtml = "<b><em>Palindrome Outcome:</em></b>  <strong>" + input + "</strong> no, not a Palindrome String";
            }
        }
         
    }
}