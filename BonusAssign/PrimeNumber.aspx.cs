﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssign
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void submitPrimeNumber_Click(object sender, EventArgs e)
        {

            if (!Page.IsValid)
            {
                return;
            }

            int primeNumberInput = int.Parse(inputPrimeNumber.Text);

            bool flag = false;

            for (int i = primeNumberInput - 1; i > 1; i--)
            {
                if (primeNumberInput % i == 0)
                {
                    flag = true;
                    break;
                }
            }

            if (flag == false)
            {
                primeNumberResult.InnerHtml = "<strong><em>Prime Number Outcome:</em></strong> " + primeNumberInput + "\" yes, Prime Number";
            }
            else
            {
                primeNumberResult.InnerHtml = "<strong><em>Prime Number Outcome:</em></strong> " + primeNumberInput + "\" no, not a Prime Number";
            }
        }
    }
}