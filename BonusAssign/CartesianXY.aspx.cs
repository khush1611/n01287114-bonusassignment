﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssign
{
    public partial class CartesianXY : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submitValue_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            double xaxisValue = Convert.ToDouble(xAxisInput.Text);
            double yaxisValue = Convert.ToDouble(yAxisInput.Text);

            if (xaxisValue > 0 && yaxisValue > 0)
            {
                xAxisYaxisResult.InnerHtml = "<b><em>Cartesian Outcome:</em></b>(" + xAxisInput.Text + "," + yAxisInput.Text + ") Quadrant 1";
            }
            else if (xaxisValue < 0 && yaxisValue > 0)
            {
                xAxisYaxisResult.InnerHtml = "<b><em>Cartesian Outcome:</em></b> (" + xAxisInput .Text + "," + yAxisInput.Text + ") Quadrant 2";
            }
            else if (xaxisValue < 0 && yaxisValue < 0)
            {
                xAxisYaxisResult.InnerHtml = "<b><em> Cartesian Outcome:</em></b> (" + xAxisInput.Text + "," + yAxisInput.Text + ")  Quadrant 3";
            }
            else if (xaxisValue > 0 && yaxisValue < 0)
            {
                xAxisYaxisResult.InnerHtml = "<b><em> Cartesian Outcome:</em></b> (" + xAxisInput.Text + "," + yAxisInput.Text + ")  Quadrant 4";
            }
        }

        protected void xAxisCustom(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(xAxisInput.Text) == 0)
            {
                args.IsValid = false;
            }
        }
        protected void yAxisCustom(object source, ServerValidateEventArgs args)
        {
            if (Convert.ToDouble(yAxisInput.Text) == 0)
            {
                args.IsValid = false;
            }
        }

    }
}
