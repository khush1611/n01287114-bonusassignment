﻿<%@ Page Title="Cartesian" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CartesianXY.aspx.cs" Inherits="BonusAssign.CartesianXY" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <h2><%: Title %></h2>
     <div class="jumbotron">
   
    <div>
        <asp:Label runat="server">X-axis: </asp:Label>
        <asp:TextBox runat="server" ID="xAxisInput"></asp:TextBox>
        <asp:RequiredFieldValidator ID="xAxisInputRequiredField" runat="server" ControlToValidate="xAxisInput" ErrorMessage="You must enter a value" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="xAxisRegularExpression" runat="server" ControlToValidate="xAxisInput" ErrorMessage="Entered value should be Number" ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="xAxisCustomValidtor" runat="server" ControlToValidate="xAxisInput" OnServerValidate="xAxisCustom" ErrorMessage=" Entered value for x-Axis should be non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />

        <br />
        <asp:Label runat="server">Y-axis : </asp:Label>
        <asp:TextBox runat="server" ID="yAxisInput"></asp:TextBox>
        <asp:RequiredFieldValidator ID="yAxisInputRequiredField" runat="server" ControlToValidate="yAxisInput" ErrorMessage="You must enter a value" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="yAxisRegularExpression" runat="server" ControlToValidate="yAxisInput" ErrorMessage="Entered value should be Number" ValidationExpression="[\-]?[0-9]{0,7}([\.][0-9])?$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        <asp:CustomValidator ID="yAxisCustomValidor" runat="server" ControlToValidate="yAxisInput" OnServerValidate="yAxisCustom" ErrorMessage=" Entered value for y-Axis should be non-zero number." ForeColor="Red" Display="Dynamic"></asp:CustomValidator>
        <br />

        <br />
        <asp:Button ID="submitValue" Text="SubmitCartesian" runat="server" OnClick="submitValue_Click"/>
           
        </div>
         <br /> <br />
          <div id="xAxisYaxisResult" runat="server"></div>
</div>
</asp:Content>
